  import React,{Component} from 'react'

class Quotes extends React.Component {
  state = {
      quote: "",
      randomImg: "http://i.imgflip.com/1bij.jpg",
      allQuotesImgs: []
  }

  // constructor() {
  //   super()
  //   this.state = {
  //     quote: "",
  //     randomImg: "http://i.imgflip.com/1bij.jpg",
  //     allQuotesImgs: []
  //   }
  //   this.handleChange = this.handleChange.bind(this)
  //   this.handleSubmit = this.handleSubmit.bind(this)
  // }

  componentDidMount() {
    fetch("https://api.imgflip.com/get_memes")
      .then(response => response.json())
      .then(response => {
        const {memes} = response.data
        this.setState({allQuotesImgs: memes})
      })
  }

  handleChange = (event) => {
  const {name, value} = event.target
    this.setState({ [name]: value })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const randNum = Math.floor(Math.random() * this.state.allQuotesImgs.length)
    const randQuoteImg = this.state.allQuotesImgs[randNum].url
    this.setState({randomImg: randQuoteImg})
  }
  render() {
    return (
      <div>
        <form className="quotes-form" onSubmit={this.handleSubmit}>
        <input
          type="text"
          name="quote"
          placeholder="Enter your quote"
          value= {this.state.quote}
          onChange={this.handleChange}
        />

        <button>Create</button>
        </form>
        <div className="quote-banner">
          <img src={this.state.randomImg} alt=""/>
          <h3>{this.state.quote}</h3>
        </div>
      </div>
    )
  }
}

export default Quotes