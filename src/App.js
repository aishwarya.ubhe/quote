import React from 'react';
import logo from './logo.svg';
import Header from './Header'
import Quotes from './Quotes'
import './App.css';

function App() {
  return (
    <div>
      <Header />
      <Quotes />
      </div>
  )
}

export default App;
